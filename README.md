* This is SPL library for my
    [STM8S toolchain](https://gitlab.com/spseol/mit-no/STM8S-toolchain).
* [SPL](SPL) is for [sdcc-gas](https://github.com/XaviDCR92/sdcc-gas)
* [SPLSPL](SPLSPL) is for elimination problems with dead code in clasic
    [SDCC](https://sdcc.sourceforge.net).
